package ua.com.epam.factory;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ua.com.epam.configurations.PropertiesManager;

import java.util.concurrent.TimeUnit;

public class DriverFactory {
    private static final Logger logger = LogManager.getLogger(DriverFactory.class);

    protected static WebDriver createDriver() {
        System.setProperty(PropertiesManager.getChromeDriver(), PropertiesManager.getChromeDriverPath());
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(PropertiesManager.getImplicitTime(), TimeUnit.SECONDS);
        logger.info("Driver was successfully created");
        return driver;
    }

    private DriverFactory() {
    }
}

package ua.com.epam.constatnts;

public class Constants {
    private Constants() {
    }

    public static final String PROPERTIES_FILE_PATH = "src/main/resources/config.properties";
    public static final String SUCCESSFULLY_SENT_MESSAGE = "Message sent.";
    public static final String RECIPIENT_XPATH = "//div[@tabindex]//span[@data-hovercard-owner-id or @data-hovercard-id]";
    public static final String ATTRIBUTE_ARIA_LABEL = "aria-label";
}


package ua.com.epam.ui.pages;

import org.openqa.selenium.By;
import ua.com.epam.decorator.elements.Button;
import ua.com.epam.factory.DriverProvider;

public class DraftsPage extends AbstractPage {

    public Button getMessageBySubject(String subject) {
        By locator = By.xpath(String.format("//div[@role='link']//span[text()='%s']/ancestor::tr", subject));
        return new Button(DriverProvider.getDriver().findElement(locator), locator);
    }
}

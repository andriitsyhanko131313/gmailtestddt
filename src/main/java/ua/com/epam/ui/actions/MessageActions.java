package ua.com.epam.ui.actions;


import io.qameta.allure.Step;
import ua.com.epam.ui.pages.DraftsPage;
import ua.com.epam.ui.pages.HomePage;
import ua.com.epam.ui.pages.NewMessagePage;
import ua.com.epam.utils.Letter;


public class MessageActions {
    private final HomePage homePage;
    private final DraftsPage draftsPage;
    private final NewMessagePage newMessagePage;

    public MessageActions() {
        homePage = new HomePage();
        draftsPage = new DraftsPage();
        newMessagePage = new NewMessagePage();
    }

    @Step("Fill in new message")
    public void fillInNewMessage(Letter letter) {
        homePage.clickComposeButton();
        newMessagePage.setRecipientsField(letter.getRecipient());
        newMessagePage.setSubjectField(letter.generateSubject());
        newMessagePage.setMessageField(letter.getMessage());
    }

    @Step("Close message form")
    public void closeMessageForm() {
        newMessagePage.clickCloseButton();
    }

    @Step("Open message with subject [{subject}]")
    public void openMessage(String subject) {
        draftsPage.getMessageBySubject(subject).saveClick();
    }

    @Step("Open drafts")
    public void openDrafts() {
        homePage.setSearchEmailField("in:draft");
    }

    @Step("Send message")
    public void sendMessage() {
        newMessagePage.clickSendMessageButton();
    }
}

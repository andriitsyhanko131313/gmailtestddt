package ua.com.epam.utils;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ua.com.epam.configurations.PropertiesManager;
import ua.com.epam.decorator.PageElement;
import ua.com.epam.factory.DriverProvider;

public class WaitUtils {
    private WaitUtils() {
    }

    private static WebDriverWait getWait() {
        return new WebDriverWait(DriverProvider.getDriver(), PropertiesManager.getExplicitTime());
    }

    public static void waitForElementToBeEnabled(PageElement pageElement) {
        getWait()
                .ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.and(el -> el.findElement(pageElement.getLocator())
                        .isEnabled(), ExpectedConditions.visibilityOf(pageElement)));
    }

    public static void waitForPageLoadComplete() {
        new WebDriverWait(DriverProvider.getDriver(), PropertiesManager.getExplicitTime()).until(
                webDriver -> ((JavascriptExecutor) webDriver)
                        .executeScript("return document.readyState").equals("complete"));
    }

    public static Boolean waitTextToBe(By locator, String text) {
        return getWait().until(ExpectedConditions.textToBe(locator, text));
    }

    public static WebElement waitForElementPresence(By locator) {
        return getWait()
                .ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public static WebElement elementToBeClickable(PageElement pageElement) {
        return getWait()
                .ignoring(StaleElementReferenceException.class)
                .until(ExpectedConditions.elementToBeClickable(pageElement));
    }

    public static void waitForVisibility(PageElement pageElement) {
        getWait().until(ExpectedConditions.visibilityOfElementLocated(pageElement.getLocator()));
    }
}
